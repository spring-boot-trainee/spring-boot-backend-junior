package com.sleepy.springbootbackendjunior;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.sleepy.business.EmailBusiness;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EmailBusinessTest {
	@Autowired
	private EmailBusiness emailBusiness;

	@Order(1)
	@Test
	void testSendActivateEmail() {
		emailBusiness.sendActivateUserEmail(TestData.email, TestData.name, TestData.token);
	}

	interface TestData {

		String email = "sleepy.reply@gmail.com"; // using send mail to

		String name = "sleepy";

		String token = "token-example-1234";

	}
}
