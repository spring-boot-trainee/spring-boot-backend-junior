package com.sleepy.springbootbackendjunior;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.annotation.CreatedBy;

import com.sleepy.entity.User;
import com.sleepy.model.MRegisterRequest;
import com.sleepy.service.UserService;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserServiceTest {

	@Autowired
	private UserService userService;

	@Order(1)
	@Test
	void create() {
		// set data
		MRegisterRequest mRegisterRequest = new MRegisterRequest();
		mRegisterRequest.setEmail(TestCreateData.email);
		mRegisterRequest.setPassword(TestCreateData.password);
		mRegisterRequest.setName(TestCreateData.name);

		// call service create
		User user = userService.create(mRegisterRequest);

		// check not null
		Assertions.assertNotNull(user);
		Assertions.assertNotNull(user.getId());

		// check equals
		Assertions.assertEquals(TestCreateData.email, user.getEmail());
		Assertions.assertEquals(TestCreateData.name, user.getName());

		// check password
		boolean isPassMatched = userService.matchPassword(TestCreateData.password, user.getPassword());
		Assertions.assertTrue(isPassMatched);

	}

	@Order(2)
	@Test
	void update() {
		Optional<User> opt = userService.findByEmail(TestCreateData.email);
		Assertions.assertTrue(opt.isPresent());

		User user = opt.get();
		User updatedUser = userService.updateName(user.getId(), TestUpdateData.name);

		Assertions.assertNotNull(updatedUser);
	}

	@Test
	void delete() {
		Optional<User> opt = userService.findByEmail(TestCreateData.email);
		Assertions.assertTrue(opt.isPresent());

		User user = opt.get();
		// remove data test
		userService.deleteById(user.getId());
	}

	interface TestCreateData {

		String email = "example@gmail.com";

		String password = "1234";

		String name = "sleepy";

	}

	interface TestUpdateData {

		String name = "sleepy";

	}

}
