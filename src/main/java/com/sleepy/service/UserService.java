package com.sleepy.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.sleepy.business.EmailBusiness;
import com.sleepy.entity.Address;
import com.sleepy.entity.User;
import com.sleepy.exception.BadRequestException;
import com.sleepy.exception.NotFoundException;
import com.sleepy.model.MRegisterRequest;
import com.sleepy.repository.AddressRepository;
import com.sleepy.repository.UserRepository;
import com.sleepy.utils.SecurityUtil;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class UserService {
	private final PasswordEncoder passwordEncoder;

	private final UserRepository userRepository;

	private final EmailBusiness emailBusiness;

	public UserService(PasswordEncoder passwordEncoder, UserRepository userRepository, EmailBusiness emailBusiness) {
		this.passwordEncoder = passwordEncoder;
		this.userRepository = userRepository;
		this.emailBusiness = emailBusiness;
	}

	@Autowired
	private AddressRepository addressRepository;

//	@Autowired
//	private UserRepository userRepository;

	/* TODO: register user */
	public User create(MRegisterRequest request) {
		if (request.getPassword() == null) {
			throw new NotFoundException("password.not.found");
		}

		if (request.getEmail() == null) {
			throw new BadRequestException("register.email.null");
		}

		if (userRepository.existsByEmail(request.getEmail())) {
			throw new BadRequestException("register.email.exits");
		}
		// create model
		User user = new User();
		user.setEmail(request.getEmail());
		user.setPassword(passwordEncoder.encode(request.getPassword()));
		user.setName(request.getName());
		user.setToken(SecurityUtil.generateToken()); // token verify email
		user.setTokenExpire(nextXMinute(30)); // set token exp in 30 min
		user.setActivated(false);
		User userRes = userRepository.save(user);

		// insert address
		Address address = new Address();
		address.setUser(userRes);
		address.setLine1("line1");
		address.setLine2("line2");
		addressRepository.save(address);

		// todo send mail
		this.sendEmail(user);

		return userRes;
	}

	public Date nextXMinute(int minute) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, minute);
		return calendar.getTime();
	}

	public void sendEmail(User user) {
		String token = user.getToken();
		emailBusiness.sendActivateUserEmail(user.getEmail(), user.getName(), token);

	}

	public User update(User user) {
		return userRepository.save(user);
	}

	/* @note: remove cache by user id */
	@CacheEvict(value = "user", key = "#id")
	public void deleteById(int id) {
		userRepository.deleteById(id);
	}

	/* @note: remove cache all by value user */
	@CacheEvict(value = "user", allEntries = true)
	public void deleteAll() {
		 userRepository.deleteAll();
	}

	public List<User> getUsers() {
		return (List<User>) userRepository.findAll();
	}

	/* @Note key #id = findById(Integer id) and make cache when result != null */
	@Cacheable(value = "user", key = "#id", unless = "#result == null")
	public Optional<User> findById(Integer id) {
		log.info("Load User From Database: " + id);
		return userRepository.findById(id);
	}

	public Optional<User> findByToken(String token) {
		return userRepository.findByToken(token);
	}

	@CachePut(value = "user", key = "#id")
	public User updateName(int id, String name) {
		Optional<User> opt = userRepository.findById(id);
		if (!opt.isPresent()) {
			throw new NotFoundException("user.not.found");
		}

		User user = opt.get();
		user.setName(name);

		return userRepository.save(user);
	}

	public Optional<User> findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	public boolean matchPassword(String rawPassword, String encodedPassword) {
		return passwordEncoder.matches(rawPassword, encodedPassword);
	}

}
