package com.sleepy.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sleepy.business.UserBusiness;
import com.sleepy.entity.Address;
import com.sleepy.entity.User;
import com.sleepy.mapper.UserMapper;
import com.sleepy.model.MActivateRequest;
import com.sleepy.model.MActivateResponse;
import com.sleepy.model.MLoginRequest;
import com.sleepy.model.MLoginResponse;
import com.sleepy.model.MRegisterRequest;
import com.sleepy.model.MRegisterResponse;
import com.sleepy.model.MResendActivationEmailRequest;
import com.sleepy.model.MUpdateUserProfileRequest;
import com.sleepy.model.MUserProfile;
import com.sleepy.service.AddressService;
import com.sleepy.service.UserService;

import scala.annotation.meta.getter;

@RestController
@RequestMapping("/user")
public class UserController {

	private final UserBusiness business;

	private final UserService userService;

	private final AddressService addressService;

	private final UserMapper userMapper;

	public UserController(UserService userService, UserMapper userMapper, UserBusiness business,
			AddressService addressService) {
		this.business = business;
		this.userService = userService;
		this.addressService = addressService;
		this.userMapper = userMapper;
	}

	/* @note using redis cache */
	@GetMapping("/profile")
	public ResponseEntity<MUserProfile> getMyUserProfile() {
		MUserProfile response = business.getMyUserProfile();
		return ResponseEntity.ok(response);
	}

	/* @note using redis cache */
	@PutMapping("/profile")
	public ResponseEntity<MUserProfile> updateMyUserProfile(@RequestBody MUpdateUserProfileRequest request) {
		MUserProfile response = business.updateMyUserProfile(request);
		return ResponseEntity.ok(response);
	}

	/* @note using redis cache */
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable int id) {
		userService.deleteById(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@PostMapping("/register")
	public ResponseEntity<MRegisterResponse> register(@RequestBody MRegisterRequest body) {
		User response = userService.create(body);
		// using mapper
		return ResponseEntity.ok(userMapper.toRegisterResponse(response));
	}

	@PostMapping("/login")
	public ResponseEntity<MLoginResponse> login(@RequestBody MLoginRequest request) {
		MLoginResponse response = business.login(request);
		return ResponseEntity.ok(response);

	}

	@GetMapping("/all")
	public ResponseEntity<?> users() {
		List<User> usersList = userService.getUsers();
		// if you want mapper can use https://www.baeldung.com/java-modelmapper-lists
		return ResponseEntity.ok(userMapper.toGetUsersResponse(usersList));
	}

	@GetMapping("/address")
	public ResponseEntity<?> usersaddress() {
		List<Address> addresses = addressService.findAddressAll();
		addresses.forEach(Item -> {
			try {
				System.out.println(Item.getUser().getEmail());
			} catch (Exception e) {
				// TODO: handle exception
			}
		});
		return ResponseEntity.ok(addresses);
	}

	// activate
	@PostMapping("/activate")
	public ResponseEntity<MActivateResponse> activate(@RequestBody MActivateRequest request) {
		MActivateResponse response = business.activate(request);
		return ResponseEntity.status(200).body(response);
	}

	// resend-activation-email
	@PostMapping("/resend-activation-email")
	public ResponseEntity<Void> resendActivationEmail(@RequestBody MResendActivationEmailRequest request) {
		business.resendActivationEmail(request);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@GetMapping("/refresh-token")
	public ResponseEntity<?> refreshToken() {
		String tokenString = business.refreshToken();
		return ResponseEntity.ok(tokenString);
	}

}
