package com.sleepy.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sleepy.business.ChatBusiness;
import com.sleepy.model.ChatMessageRequest;

@RestController
@RequestMapping("/chat")
public class ChatController {

	private final ChatBusiness business;

	public ChatController(ChatBusiness business) {
		super();
		this.business = business;
	}

	@PostMapping("/message")
	public ResponseEntity<Void> post(@RequestBody ChatMessageRequest request) {
		business.post(request);
		return ResponseEntity.status(HttpStatus.OK).build();
	}
}
