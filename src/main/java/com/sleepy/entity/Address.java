package com.sleepy.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Data
@Entity
@Table(name = "address")
public class Address implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(length = 120)
	private String line1;

	@Column(length = 120)
	private String line2;

	@Column(length = 120)
	private String zipcode;

	@JsonBackReference // solve the infinite recursion problem without ignoring the getters/setters
						// during serialization: @JsonManagedReference and @JsonBackReference.
	@ManyToOne
	@JoinColumn(name = "users_id", nullable = false)
	private User user;

}
