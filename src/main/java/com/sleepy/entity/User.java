package com.sleepy.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Data
@Entity
@Table(name = "users")
/* @Note implements Serializable for using cache */
public class User implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false, unique = true, length = 60)
	private String email;
	private String password;
	private String name;

	@JsonManagedReference // solve the infinite recursion problem without ignoring the getters/setters
							// during serialization: @JsonManagedReference and @JsonBackReference.
	@OneToMany(mappedBy = "user", orphanRemoval = true, fetch = FetchType.EAGER)
	private List<Address> address;

	@Column(nullable = true)
	private String civilId;
	@Column(nullable = true)
	private String token;
	@Column(nullable = true)
	private Date tokenExpire;
	@Column(nullable = true)
	private boolean activated;

}
