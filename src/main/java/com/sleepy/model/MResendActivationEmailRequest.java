package com.sleepy.model;

import lombok.Data;

@Data
public class MResendActivationEmailRequest {
	private String token;
}
