package com.sleepy.model;

import lombok.Data;

@Data
public class MLoginResponse {
	private String token;
}
