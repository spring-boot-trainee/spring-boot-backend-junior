package com.sleepy.model;

import lombok.Data;

@Data
public class MActivateRequest {
	private String token;
}
