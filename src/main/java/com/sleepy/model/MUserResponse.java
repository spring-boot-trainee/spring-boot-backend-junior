package com.sleepy.model;

import java.util.List;

import com.sleepy.entity.Address;

import lombok.Data;

@Data
public class MUserResponse {
	private Integer id;
	private String email;
	private String name;
	private List<Address> address;
}
