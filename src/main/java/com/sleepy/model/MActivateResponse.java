package com.sleepy.model;

import lombok.Data;

@Data
public class MActivateResponse {
	private boolean success;
}
