package com.sleepy.model;

import lombok.Data;

@Data
public class MUpdateUserProfileRequest {
	private String name;
}
