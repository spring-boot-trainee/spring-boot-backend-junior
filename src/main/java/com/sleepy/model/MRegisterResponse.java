package com.sleepy.model;

import lombok.Data;

@Data
public class MRegisterResponse {
	private String email;
	private String name;
	private String note = "xx";
}
