package com.sleepy.model;

import lombok.Data;

@Data
public class MUserProfile {

	private String name;

	private String email;

}