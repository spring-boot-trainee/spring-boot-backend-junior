package com.sleepy.model;

import java.util.Date;

import lombok.Data;

@Data
public class ChatMessage {
	private String from;

	private String message;

	private Date created;

	public ChatMessage() {
		this.created = new Date();
	}
}
