package com.sleepy.model;

import lombok.Data;

@Data
public class ChatMessageRequest {
	private String message;
}
