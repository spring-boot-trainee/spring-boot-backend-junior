package com.sleepy.config.token;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.ObjectUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.sleepy.service.TokenService;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class TokenFilter extends GenericFilterBean {

	private final TokenService tokenService;

	public TokenFilter(TokenService tokenService) {
		super();
		this.tokenService = tokenService;
	}

	// using extends GenericFilterBean
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		String authorization = request.getHeader("Authorization");

		if (ObjectUtils.isEmpty(authorization)) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;
		}

		if (!authorization.startsWith("Bearer ")) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;
		}

		String tokenString = authorization.substring(7);
		DecodedJWT decoded = tokenService.verify(tokenString);
		log.info(decoded);

		if (decoded == null) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;
		}

		// user id
		Integer principal = decoded.getClaim("principal").asInt();
		String role = decoded.getClaim("role").asString();

		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(role));

		// set authentication
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(principal,
				"(protected)", authorities);

		// set SecurityContext
		SecurityContext context = SecurityContextHolder.getContext();
		context.setAuthentication(authentication);

		filterChain.doFilter(servletRequest, servletResponse);

	}

	// using extends OncePerRequestFilter
//	@Override
//	protected void doFilterInternal(HttpServletRequest servletRequest, HttpServletResponse servletResponse,
//			FilterChain filterChain) throws ServletException, IOException {
//		log.info("doFilterInternal");
//
//		String authorization = servletRequest.getHeader("Authorization");
//
//		if (ObjectUtils.isEmpty(authorization)) {
//			filterChain.doFilter(servletRequest, servletResponse);
//			return;
//		}
//
//		if (!authorization.startsWith("Bearer ")) {
//			filterChain.doFilter(servletRequest, servletResponse);
//			return;
//		}
//
//		String tokenString = authorization.substring(7);
//		DecodedJWT decoded = tokenService.verify(tokenString);
//		log.info(decoded);
//
//		if (decoded == null) {
//			filterChain.doFilter(servletRequest, servletResponse);
//			return;
//		}
//
//		// user id
//		Integer principal = decoded.getClaim("principal").asInt();
//		String role = decoded.getClaim("role").asString();
//
//		List<GrantedAuthority> authorities = new ArrayList<>();
//		authorities.add(new SimpleGrantedAuthority(role));
//
//		// set authentication
//		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(principal,
//				"(protected)", authorities);
//
//		// set SecurityContext
//		SecurityContext context = SecurityContextHolder.getContext();
//		context.setAuthentication(authentication);
//		filterChain.doFilter(servletRequest, servletResponse);
//
//	}

}
