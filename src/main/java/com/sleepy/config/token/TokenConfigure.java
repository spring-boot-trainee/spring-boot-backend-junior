package com.sleepy.config.token;

import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.sleepy.service.TokenService;

public class TokenConfigure extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

	private final TokenService service;

	public TokenConfigure(TokenService service) {
		super();
		this.service = service;
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.addFilterBefore(new TokenFilter(service), UsernamePasswordAuthenticationFilter.class);
	}
}
