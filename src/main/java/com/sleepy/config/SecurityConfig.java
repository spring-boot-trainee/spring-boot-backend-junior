package com.sleepy.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.sleepy.config.token.TokenConfigure;
import com.sleepy.service.TokenService;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private final TokenService tokenService;

	private final String[] PUBLIC = { "/actuator/**", "/user/register", "/user/login", "/user/activate",
			"/user/resend-activation-email", "/socket/**", "/swagger-ui-custom.html", "/swagger-ui/**",
			"/v3/api-docs/**" };

	public SecurityConfig(TokenService tokenService) {
		super();
		this.tokenService = tokenService;
	}

	@Bean
	public PasswordEncoder PasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// TODO Auto-generated method stub
		super.configure(auth);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
		http.cors(config -> {
			CorsConfiguration cors = new CorsConfiguration();
			cors.setAllowCredentials(true);
			cors.setAllowedOriginPatterns(Collections.singletonList("http://*"));
			cors.addAllowedHeader("*");
			cors.addAllowedMethod("GET");
			cors.addAllowedMethod("POST");
			cors.addAllowedMethod("PUT");
			cors.addAllowedMethod("DELETE");
			cors.addAllowedMethod("OPTIONS");

			UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
			source.registerCorsConfiguration("/**", cors);

			config.configurationSource(source);
		}).csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
				.authorizeRequests().antMatchers(PUBLIC).anonymous().anyRequest().authenticated().and()
				.apply(new TokenConfigure(tokenService));
	}
}
