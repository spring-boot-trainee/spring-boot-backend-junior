package com.sleepy.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.sleepy.entity.User;
import com.sleepy.model.MRegisterResponse;
import com.sleepy.model.MUserProfile;
import com.sleepy.model.MUserResponse;

@Mapper(componentModel = "spring")
public interface UserMapper {

	// map class User to MRegisterResponse
	MRegisterResponse toRegisterResponse(User user);

	MUserProfile toUserProfile(User user);

	List<MUserResponse> toGetUsersResponse(List<User> user);
}
