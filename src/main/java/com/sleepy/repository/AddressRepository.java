package com.sleepy.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.sleepy.entity.Address;
import com.sleepy.entity.User;

public interface AddressRepository extends CrudRepository<Address, Integer> {
	List<Address> findByUser(User user);
}
