package com.sleepy.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sleepy.entity.User;

public interface UserRepository extends CrudRepository<User, Integer> {
	Optional<User> findByEmail(String email);
	
	Optional<User> findByToken(String token);

	boolean existsByEmail(String email);
}
