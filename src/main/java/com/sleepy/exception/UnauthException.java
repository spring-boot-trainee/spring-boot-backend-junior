package com.sleepy.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class UnauthException extends RuntimeException {

	public UnauthException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UnauthException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public UnauthException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UnauthException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UnauthException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
