package com.sleepy.exception;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.Data;

@ControllerAdvice
public class ControllerErrorAdviser extends ResponseEntityExceptionHandler {

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<?> handleBadRequestException(BadRequestException e, WebRequest request) {
		ErrorResponse response = new ErrorResponse();
		response.setError(e.getMessage());
		response.setStatus(400);
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(UnauthException.class)
	public ResponseEntity<ErrorResponse> handleUnauthorizedException(UnauthException e, WebRequest request) {
		ErrorResponse response = new ErrorResponse();
		response.setError(e.getMessage());
		response.setStatus(401);
		return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
	}

//	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<?> resourceNotFoundException(NotFoundException e, WebRequest request) {
		ErrorResponse response = new ErrorResponse();
		response.setError(e.getMessage());
		response.setStatus(404);
		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	}

	@Data
	public static class ErrorResponse {

		private LocalDateTime timestamp = LocalDateTime.now();
		private int status;
		private String error;

	}
}
