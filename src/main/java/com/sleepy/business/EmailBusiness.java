package com.sleepy.business;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.common.EmailRequest;
import com.sleepy.exception.NotFoundException;
import com.sleepy.service.EmailService;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class EmailBusiness {

	private final EmailService emailService;

	private final KafkaTemplate<String, EmailRequest> kaflaKafkaTemplate;

	public EmailBusiness(EmailService emailService, KafkaTemplate<String, EmailRequest> kaflaKafkaTemplate) {
		super();
		this.emailService = emailService;
		this.kaflaKafkaTemplate = kaflaKafkaTemplate;
	}

	public void sendActivateUserEmail(String email, String name, String token) {
		// prepare content (HTML)
		String html;
		try {
			html = readEmailTemplate("email-activate-user.html");
		} catch (IOException e) {
			throw new NotFoundException("template.email.not.found");
		}

		String finalLink = "http://localhost:4200/activate/" + token;
		html = html.replace("${P_NAME}", name);
		html = html.replace("${P_LINK}", finalLink);
		final String subject = "กรุณา activate บัญชี";

		// emailService.send(email, subject, html);

		// ex using kafka
		EmailRequest request = new EmailRequest();
		request.setTo(email);
		request.setSubject(subject);
		request.setContent(html);
		System.out.println(request);

		ListenableFuture<SendResult<String, EmailRequest>> future = kaflaKafkaTemplate.send("topic-activation-email",
				request);
		future.addCallback(new ListenableFutureCallback<SendResult<String, EmailRequest>>() {

			@Override
			public void onSuccess(SendResult<String, EmailRequest> result) {
				log.info("kafka onSuccess");
				log.info(result);

			}

			@Override
			public void onFailure(Throwable ex) {
				log.error("kafka onFailure");
				log.error(ex);

			}
		});

	}

	private String readEmailTemplate(String filename) throws IOException {
		File file = ResourceUtils.getFile("classpath:email/" + filename);
		return FileCopyUtils.copyToString(new FileReader(file));
	}
}
