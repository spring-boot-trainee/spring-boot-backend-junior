package com.sleepy.business;

import java.util.Date;
import java.util.Optional;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.sleepy.entity.User;
import com.sleepy.exception.BadRequestException;
import com.sleepy.exception.NotFoundException;
import com.sleepy.exception.UnauthException;
import com.sleepy.mapper.UserMapper;
import com.sleepy.model.MActivateRequest;
import com.sleepy.model.MActivateResponse;
import com.sleepy.model.MLoginRequest;
import com.sleepy.model.MLoginResponse;
import com.sleepy.model.MResendActivationEmailRequest;
import com.sleepy.model.MUpdateUserProfileRequest;
import com.sleepy.model.MUserProfile;
import com.sleepy.service.TokenService;
import com.sleepy.service.UserService;
import com.sleepy.utils.SecurityUtil;

import io.netty.util.internal.StringUtil;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class UserBusiness {
	private final UserService userService;

	private final TokenService tokenService;

	private final UserMapper userMapper;

	public UserBusiness(UserService userService, TokenService tokenService, UserMapper userMapper) {
		super();
		this.userService = userService;
		this.tokenService = tokenService;
		this.userMapper = userMapper;
	}

	public MUserProfile updateMyUserProfile(MUpdateUserProfileRequest request) {
		Optional<Integer> opt = SecurityUtil.getCurrentUserId();
		if (!opt.isPresent()) {
			throw new UnauthException("unauthorized!!!!");
		}

		Integer userId = opt.get();

		// validate
		if (ObjectUtils.isEmpty(request.getName())) {
			throw new BadRequestException("name.null");
		}

		User user = userService.updateName(userId, request.getName());

		return userMapper.toUserProfile(user);
	}

	public MUserProfile getMyUserProfile() {
		Optional<Integer> opt = SecurityUtil.getCurrentUserId();
		if (!opt.isPresent()) {
			throw new UnauthException("unauthorized!!!!");
		}

		Integer userId = opt.get();

		Optional<User> optUser = userService.findById(userId);
		if (!optUser.isPresent()) {
			throw new NotFoundException("user.not.found");
		}

		return userMapper.toUserProfile(optUser.get());

	}

	public MLoginResponse login(MLoginRequest request) {
		// validate request
		if (request.getPassword() == null) {
			throw new UnauthException("unauthorized!!!!");
		}

		// verify database
		Optional<User> userOptional = userService.findByEmail(request.getEmail());
		if (!userOptional.isPresent()) {
			throw new UnauthException("unauthorized!!!!");
		}

		User user = userOptional.get();
		// verify password
		if (!userService.matchPassword(request.getPassword(), user.getPassword())) {
			throw new UnauthException("unauthorized!!!!");
		}

		// verify activate status
		if (!user.isActivated()) {
			throw new UnauthException("login.fail");
		}

		String tokenString = tokenService.generateToken(user);

		// set response
		MLoginResponse response = new MLoginResponse();
		response.setToken(tokenString);
		return response;

	}

	public String refreshToken() {
		Optional<Integer> opt = SecurityUtil.getCurrentUserId();

		if (!opt.isPresent()) {
			throw new UnauthException("unauthorized!!!!");
		}

		Integer userId = opt.get();

		Optional<User> optUser = userService.findById(userId);
		if (!optUser.isPresent()) {
			throw new NotFoundException("user.not.found");
		}

		User user = optUser.get();
		return tokenService.generateToken(user);

	}

	public MActivateResponse activate(MActivateRequest request) {
		String token = request.getToken();
		if (StringUtil.isNullOrEmpty(token)) {
			throw new BadRequestException("activate.no.token");
		}

		Optional<User> opt = userService.findByToken(token);
		if (!opt.isPresent()) {
			throw new BadRequestException("activate.fail");
		}

		User user = opt.get();

		if (user.isActivated()) {
			throw new BadRequestException("activate.already");
		}

		Date nowDate = new Date();
		Date expireDate = user.getTokenExpire();
		if (nowDate.after(expireDate)) {
			throw new BadRequestException("activate.token.exp");
		}

		// update user activated
		user.setActivated(true);
		userService.update(user);

		// response
		MActivateResponse response = new MActivateResponse();
		response.setSuccess(true);

		return response;
	}

	public void resendActivationEmail(MResendActivationEmailRequest request) {
		String token = request.getToken();
		if (StringUtil.isNullOrEmpty(token)) {
			throw new BadRequestException("resend.activate.no.token");
		}
		log.info(request);
		Optional<User> opt = userService.findByToken(token);
		if (!opt.isPresent()) {
			throw new BadRequestException("resend.activate.fail");
		}

		User user = opt.get();

		if (user.isActivated()) {
			throw new BadRequestException("activate.already");
		}

		user.setToken(SecurityUtil.generateToken());
		user.setTokenExpire(userService.nextXMinute(30));
		user = userService.update(user);

		userService.sendEmail(user);
	}

}
