package com.sleepy.business;

import java.util.Optional;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.sleepy.exception.UnauthException;
import com.sleepy.model.ChatMessage;
import com.sleepy.model.ChatMessageRequest;
import com.sleepy.utils.SecurityUtil;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class ChatBusiness {
	private final SimpMessagingTemplate template;

	public ChatBusiness(SimpMessagingTemplate template) {
		super();
		this.template = template;
	}

	public void post(ChatMessageRequest request) {

		Optional<Integer> opt = SecurityUtil.getCurrentUserId();

		if (!opt.isPresent()) {
			throw new UnauthException("unauthorized");
		}

		final String destination = "/topic/chat";

		ChatMessage payload = new ChatMessage();
		payload.setFrom("From");
		payload.setMessage(request.getMessage());

		template.convertAndSend(destination, payload);
		log.info("post message");
	}

}
