# เริ่มต้น project spring boot (java) backend
....

## Installation

### 1. sql server

```bash
docker run --name mssqlserver -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=<pssword>' -p 1433:1433 -d mcr.microsoft.com/mssql/server:2017-latest-ubuntu

# password ต้องกำหนดตัวเล็กตัวใหญ่อย่างน้อย 8 ตัว
# read more: https://hub.docker.com/_/microsoft-mssql-server
```

### 2. Redis

```bash

docker run --name redis -p 6379:6379 --privileged=true --restart=always  -itd  redis  --requirepass  "P@ssword"

# tool manage redis: [RedisInsight – the best Redis GUI](https://redislabs.com/redis-enterprise/redis-insight/)

#read more: https://hub.docker.com/_/redis
```

### 3. kafka

```bash
#Step 1
docker network create app-backend --driver bridge
#Step 2
docker run -d --name zookeeper --network app-backend -p 2181:2181 -e ALLOW_ANONYMOUS_LOGIN=yes bitnami/zookeeper
#Step 3
docker run -d --name kafka --network app-backend --hostname localhost -p 9092:9092 -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_CFG_ZOOKEEPER_CONNECT=zookeeper:2181 bitnami/kafka

#read more: https://hub.docker.com/r/bitnami/kafka
```

## Note
- Example Multi-Module Spring Boot Project in Spring Tool Suite (STS) [Link](https://www.youtube.com/watch?v=Wdjwyv0lzQU)



## Deployyment

### deploy registry สำหรับเก็บ docker image
```bash
docker pull registry
docker run -d -p 5000:5000 --restart always --name registry registry

# ตัวอย่างใช้ต้อง tag image ไปเป็นชื่อใหม่ => docker tag spring-boot-backend-junior:1.0 localhost:5000/spring-boot-backend-junior:1.0
```
